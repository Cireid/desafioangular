import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MunicipioComponent } from './pages/municipio/municipio.component';
import { DistribuidoraComponent } from './pages/distribuidora/distribuidora.component';
import { CompraVendaComponent } from './pages/compra-venda/compra-venda.component';
import { HomeComponent } from './pages/home/home.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { MunicipioService } from './shared/services/municipio.service';
import { DistribuidoraService } from './shared/services/distribuidora.service';
import { CompraVendaService } from './shared/services/compra-venda.service';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HistoricoComponent } from './pages/historico/historico.component';
import ptBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';

registerLocaleData(ptBr);

@NgModule({
  declarations: [
    AppComponent,
    MunicipioComponent,
    DistribuidoraComponent,
    CompraVendaComponent,
    HomeComponent,
    HistoricoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [
    MunicipioService,
    DistribuidoraService,
    CompraVendaService,
    { provide: LOCALE_ID, useValue: 'pt' },
    {provide: DEFAULT_CURRENCY_CODE, useValue: 'BRL'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
