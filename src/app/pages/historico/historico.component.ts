import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HistoricoService } from 'src/app/shared/services/historico.service';
import * as moment from 'moment';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.scss']
})
export class HistoricoComponent implements OnInit {

  form: FormGroup;
  model: any;
  modelid: any;
  pesquisar: any;
  p:number = 1;
  visualizar: boolean = false;

  constructor(
    private service: HistoricoService,
    private fb: FormBuilder,
    ) {
      this.form = new FormGroup({});
    }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: this.fb.control('0'),
      combustivel: this.fb.control('', [Validators.required]),
      data: this.fb.control('' , [Validators.required]),
      preco: this.fb.control('', [Validators.required])
    })
    this.listar()
  }

  listar() {
    this.service.listar()
      .then((response) => {
        this.model = response
      })
      .catch((error) => {
        console.log(error)
      });
  }

  inserir() {
    if (this.form.invalid){
      Swal.fire({
        icon: 'error',
        title: 'Algo deu errado',
        text: 'Dados do formulario são invalidos',
        showConfirmButton: false,
        timer: 2000,
      })
      return;
    }

    const form = {
      ...this.form.value,
      data: moment(this.form.value.data).format('DD/MM/YYYY')
    }
    this.service.inserir(form)
      .then(() => {
        Swal.fire({
          icon: 'success',
          title: 'Sucesso!!',
          text: 'Item adicionado com sucesso',
          showConfirmButton: false,
          timer: 2000,
        })
        this.listar();
        this.form.reset();
      })
      .catch((error) => {
        Swal.fire({
          icon: 'error',
          title: 'Algo deu errado',
          text: 'Entre em contato com o support',
          showConfirmButton: false,
          timer: 2500,
        })
        this.listar()
      })
  }

  atualizar() {
    const form = {
      ...this.form.value,
      data: moment(this.form.value.data).format('DD/MM/YYYY')
    }
    this.service.atualizar(form)
      .then(() => {
      Swal.fire({
        icon: 'success',
        title: 'Sucesso!!',
        text: 'Historico atualizado com sucesso',
        showConfirmButton: false,
        timer: 2000,
      })
    })
    .catch(() => {
      Swal.fire({
        icon: 'error',
        title: 'Algo deu errado',
        text: 'Entre em contato com o support',
        showConfirmButton: false,
        timer: 2500,
      })
    });
    this.listar();
  }

  deletar(id: any) {
    Swal.fire({
      icon:'question',
      title:'Quer mesmo apagar isso?',
      text:'Você não sera capaz de reverter isso!!',
      showCancelButton:true,
      confirmButtonColor:'red',
      cancelButtonColor:'green',
      confirmButtonText:'Apagar',
      cancelButtonText:'Voltar'
    }).then((resultado) => {
      if(resultado.isConfirmed){
        this.service.deletar(id)
        .then((response) => {
          Swal.fire({
            icon:'success',
            title:'Sucesso',
            text:'Usuario apagado com sucesso',
            timer:1200,
            showConfirmButton:false,
          })
          this.listar()
        })
        .catch(error => Swal.fire({
          icon:'error',
          title:'Algo deu errado',
          text:error.error.error,
        }))
        return;
      }
    })
      this.listar();
  }

  exibirDetalhes(id: any) {
    this.service.exibirDetalhes(id)
      .then((response) => {
        this.model = response
      })
      .catch((error) => {
        console.log(error)
      })
  }

  preencherForm(value: any, view = false) {
    const data = this.formatarData(value.data);
    this.modelid = value.id;
    this.visualizar = view;
    this.form.patchValue({
      id: value.id,
      combustivel: value.combustivel,
      data: data,
      preco: value.preco,
    })
  }

  formatarData(data: any) {
    const newData = data.split("/");
    return newData[2] + "-" + newData[1] + "-" + newData[0];
  }

  submit() {
    if (this.modelid) {
      this.atualizar();
      return
    }
    this.inserir();
  }

}