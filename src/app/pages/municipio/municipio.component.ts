import { Component, OnInit } from '@angular/core';
import { MunicipioService } from 'src/app/shared/services/municipio.service';

@Component({
  selector: 'app-municipio',
  templateUrl: './municipio.component.html',
  styleUrls: ['./municipio.component.scss']
})
export class MunicipioComponent implements OnInit {

  model: any;
  pesquisar: any;
  municipio: any;
  p: number = 1;

  constructor(
    private service: MunicipioService,
  ) { }

  ngOnInit(): void {
    this.listar();
  }

  listar() {
    this.service.listar()
      .then((response: any) => {
        this.model = response;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  mediaPrecoPorMunicipio(municipio: any) {
    this.service.mediaPreçoPorMunicipio(municipio)
      .then((response: any) => {
        this.municipio = response;
      })
      .catch((error: any) => {
        console.log(error);
      })
  }
}
