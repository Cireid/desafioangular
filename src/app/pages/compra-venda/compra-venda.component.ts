import { Component, OnInit } from '@angular/core';
import { CompraVendaService } from 'src/app/shared/services/compra-venda.service';

@Component({
  selector: 'app-compra-venda',
  templateUrl: './compra-venda.component.html',
  styleUrls: ['./compra-venda.component.scss']
})
export class CompraVendaComponent implements OnInit {

  model:any;
  pesquisar: any;
  p:number = 1;

  constructor(private service: CompraVendaService) { }

  ngOnInit(): void {
    this.listar()
  }

  listar() {
    this.service.listar()
      .then((response: any) => {
        this.model = response
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}
