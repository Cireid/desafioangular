import { Component, OnInit } from '@angular/core';
import { DistribuidoraService } from 'src/app/shared/services/distribuidora.service';

@Component({
  selector: 'app-distribuidora',
  templateUrl: './distribuidora.component.html',
  styleUrls: ['./distribuidora.component.scss']
})
export class DistribuidoraComponent implements OnInit {

  model: any;
  pesquisar: any;
  p:number = 1;

  constructor(private service: DistribuidoraService) { }

  ngOnInit(): void {
    this.listar();
  }

  listar() {
    this.service.listar()
      .then((response: any) => {
        this.model = response
      })
      .catch((error: any) => {
        console.log(error)
      })
  }
}
