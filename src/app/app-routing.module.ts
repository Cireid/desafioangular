import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompraVendaComponent } from './pages/compra-venda/compra-venda.component';
import { DistribuidoraComponent } from './pages/distribuidora/distribuidora.component';
import { HistoricoComponent } from './pages/historico/historico.component';
import { HomeComponent } from './pages/home/home.component';
import { MunicipioComponent } from './pages/municipio/municipio.component';



const routes: Routes = [
  {
    path: "", component: HomeComponent, children: [
      { path: "", redirectTo: "municipio", pathMatch: "full" },
      { path: "municipio", component: MunicipioComponent },
      { path: "distribuidora", component: DistribuidoraComponent },
      { path: "compra-venda", component: CompraVendaComponent },
      { path: "historico", component: HistoricoComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
