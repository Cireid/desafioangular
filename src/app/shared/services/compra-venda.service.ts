import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompraVendaService {

  api = environment.api

  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get(`${this.api}/combustivel/valor-media-compra-venda-municipio`).toPromise();
  }

  
}
