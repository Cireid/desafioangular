import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MunicipioService {

  api = environment.api;

  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get(`${this.api}/combustivel/dados-agrupados-por-distribuidora`).toPromise();
  }

  mediaPreçoPorMunicipio(municipio: string) {
    return this.http.get(`${this.api}/combustivel/media-de-preco/${municipio}`).toPromise();
  }
  
}
