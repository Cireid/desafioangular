import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  api = environment.api;
  config = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get(`${this.api}/historico`, this.config).toPromise();
  }

  inserir(form: any) {
    return this.http.post(`${this.api}/historico`, form, this.config).toPromise();
  }

  atualizar(form: any) {
    return this.http.put(`${this.api}/historico`, form, this.config).toPromise();
  }

  deletar(id: any) {
    return this.http.delete(`${this.api}/historico/${id}`, this.config).toPromise();
  }

  exibirDetalhes(id: any) {
    return this.http.get(`${this.api}/historico/${id}`, this.config).toPromise();
  }
}
