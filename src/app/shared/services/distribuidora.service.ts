import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DistribuidoraService {

  api = environment.api;

  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get(`${this.api}/combustivel/dados-agrupados-por-distribuidora`).toPromise();
  }
}
